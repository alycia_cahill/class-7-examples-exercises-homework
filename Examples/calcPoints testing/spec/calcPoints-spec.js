describe ("calcPoints", function() {

    it("Testing for hands", function(){
        const firstHand = [{ 
                suit: 'diamonds', 
                val: 10, 
                displayVal: '10', 
            }, 
            {
                suit: 'hearts', 
                val: 7, 
                displayVal: '7', 
            }]; 
            const secondHand = [{
                suit: 'Spades', 
                val: 11, 
                displayVal: 'Ace', 
            }, 
            {
                suit: 'clubs', 
                val: 9, 
                displayVal: '9',                                                                                       
            }];  
            const thirdHand = [{
                suit: 'clubs', 
                val: 10,
                displayVal: '10', 
            }, 
            {
                suit: 'diamonds', 
                val: 6, 
                displayVal: '6', 
            }, 
            {
                suit: 'hearts', 
                val: 11, 
                displayVal: 'Ace', 
               
            },
        ];  
        expect(determineScore(firstHand).total).toEqual(17); 
        expect(determineScore(secondHand).total).toEqual(20); 
        expect(determineScore(thirdHand).total).toEqual(17); 
    }); 
}); 