/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl 
 * @param {Event} submitEvent 
 */

const inputElements = document.getElementsByClassName('validate-input');

const validateFirstName = function(submitEvent){
  const firstName = inputElements[0];
  if (firstName.value.length <= 3){
      firstName.parentElement.classList.add('invalid'); 
      firstName.nextElementSibling.innerHTML= "The First Name must contain three or more characters"; 
      submitEvent.preventDefault(); 
      console.log ("Bad input"); 
  }else{
    firstName.parentElement.classList.remove('invalid'); 
  }
}

const validateLastName = function(submitEvent){
  const lastName = inputElements[1];
  if (lastName.value.length <3){
      lastName.parentElement.classList.add('invalid'); 
      lastName.nextElementSibling.innerHTML= "The Last Name must contain three or more characters"; 
      submitEvent.preventDefault();
      console.log ("Bad input"); 
  }else{
      lastName.parentElement.classList.remove('invalid'); 
  }
}

const validateEmail = function(submitEvent){
  const email = inputElements[1]; 
  const emailFormat = /\w+@\w+\.\w+/

  if(email.value.match(emailFormat)){
    email.parentElement.classList.remove('invalid'); 
  }else{
    email.parentElement.classList.add('invalid'); 
    email.nextElementSibling.innerHTML="Your email format is invalid."
    submitEvent.preventDefault();
    console.log ("Bad input"); 
  }

}

const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
        validateFirstName(e); 
        validateLastName(e); 
        validateEmail(e); 
      e.preventDefault();
    });
