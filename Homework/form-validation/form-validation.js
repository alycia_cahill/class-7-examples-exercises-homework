
const inputElements = document.getElementsByClassName('validate-input');

const validateFullName = function(submitEvent){
    const fullName = inputElements[0];
    if (fullName.value.length <= 3){
        fullName.nextElementSibling.innerHTML= "Name must contain three or more characters"; 
        submitEvent.preventDefault(); 
    }
  }

  const jobItems = document.getElementsByClassName('job');
  const talkItem = document.getElementById('talk');

  const checkContactKind = function(){ 
    const contactKind = document.getElementById('contact-kind'); 
    const selection = contactKind.selectedIndex;
    if (selection == 1){
        //for Job Oppurtinity 
        for(i=0; i< jobItems.length; i++){
            jobItems[i].classList.remove('d-none'); 
        }
        localStorage.setItem('contact_type', 'Job Oppurtinity'); 
    }else if(selection == 2){
        //for Talk code
        talkItem.classList.remove('d-none'); 
        localStorage.setItem('contact_type', 'Job Oppurtinity'); 
    }else{
        //do nothing 
    }
  }

  const validateEmail = function(submitEvent){
    const email = inputElements[1]; 
    const emailFormat = /\w+@\w+\.\w+/

    if(!email.value.match(emailFormat)){
        email.nextElementSibling.innerHTML="Your email format is invalid."
        submitEvent.preventDefault();
    }
  }

  const validateMessage = function(submitEvent){
    const message = inputElements[2];
    if (message.value.length <= 10){
        message.nextElementSibling.innerHTML= "Message must be 10 or more characters"; 
        submitEvent.preventDefault(); 
    }
  }

  document.addEventListener('DOMContentLoaded',function() {
    const kind = document.querySelector('select[name="kind"]').onchange=checkContactKind;
    },false);
  
  const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
        validateFullName(e); 
        validateEmail(e); 
        validateMessage(e); 
        
    });