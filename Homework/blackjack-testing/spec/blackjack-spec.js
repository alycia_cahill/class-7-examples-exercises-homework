describe ("dealerShouldDraw", function() {

    it("Test Dealer Should Draw", function(){
        const firstHand = [{ 
                suit: 'diamonds', 
                val: 10, 
                displayVal: '10', 
            }, 
            {
                suit: 'hearts', 
                val: 9, 
                displayVal: '9', 
            }]; 
            const secondHand = [{
                suit: 'Spades', 
                val: 11, 
                displayVal: 'Ace', 
            }, 
            {
                suit: 'clubs', 
                val: 6, 
                displayVal: '6',                                                                                       
            }];  
            const thirdHand = [{
                suit: 'clubs', 
                val: 10,
                displayVal: '10', 
            }, 
            {
                suit: 'diamonds', 
                val: 7, 
                displayVal: '6', 
            }]; 
            const fourthHand = [{
                suit: 'spades', 
                val: 2,
                displayVal: '2', 
            }, 
            {
                suit: 'hearts', 
                val: 4, 
                displayVal: '4', 
            }, 
            {
                suit: 'diamonds', 
                val: 2, 
                displayVal: '2', 
            },
            {
                suit: 'clubs', 
                val: 5, 
                displayVal: '5', 
            }];  
        expect(dealerShouldDraw(firstHand)).toBe(false); 
        expect(dealerShouldDraw(secondHand)).toBe(true); 
        expect(dealerShouldDraw(thirdHand)).toBe(false); 
        expect(dealerShouldDraw(fourthHand)).toBe(true); 
    }); 
}); 